var gulp = require("gulp"),
		jsLint = require("gulp-jslint"),
		jsmin = require("gulp-jsmin"),
		concat = require("gulp-concat"),
		browserSync = require('browser-sync').create(),
		clean = require('gulp-clean'),
		jsonServer = require("gulp-json-srv");

		var server = jsonServer.create({
			port: 25000,
			id:   '_id',
			baseUrl: '/',
			static: './static',
			cumulative: true,
			cumulativeSession: false
		});

		gulp.task("start", function(){
		    return gulp.src("./app/mocks/contacts.json")
		        .pipe(server.pipe());
		});

gulp.task(
	"jsLint",
	function() {
		"use strict";

		return gulp.src("./app/**/*.js")
			.pipe(jsLint())
			.on("error", function(error){
				console.error(String(error));
			});
	}
);

gulp.task("mini", function(){
	"use strict";

	gulp.src("./app/**/*.js")
		.pipe(jsmin())
		.pipe(rename({
			suffix: ".min"
		}))
		.pipe(gulp.dest("dist/js/"));
});

gulp.task("concat", function(){
	"use strict";

	gulp.src("./app/**/*.js")
		.pipe(concat(main.min.js))
		.pipe(jsmin())
		.pipe(rename({
			suffix: ".min"
		}))
		.pipe(gulp.dest("dist/js/"));
});

gulp.task("copylibs", function() {
	"use strict";

	gulp.src("./app/lib/**/*")
			.pipe(gulp.dest("dist/lib/"));
});

gulp.task('browser', function() {
  browserSync.init({
    server: {
      baseDir: './app'
    },
  });

		gulp.watch("app/index.html").on("change", browserSync.reload);
    gulp.watch("app/components/*.html").on("change", browserSync.reload);
    gulp.watch("app/views/**/*.html").on("change", browserSync.reload);

		gulp.watch("app/app.js").on("change", browserSync.reload);
		gulp.watch("app/views/**/*.js").on("change", browserSync.reload);
		gulp.watch("app/components/*.js").on("change", browserSync.reload);
		gulp.watch("app/controllers/*.js").on("change", browserSync.reload);

});

gulp.task('clean', function(){
	"use strict"

	gulp.src('.dist/**/*', {read: false})
			.pipe(clean());
});

gulp.task('install', function(){
	"use strict"

	gulp.src("./app/**/*")
			.pipe(gulp.dest("./dist/"));
});



gulp.task("default", ["clean", "install","start", "browser"]);
