'use strict';
(function() {
angular
.module('mainApp', [ 'ui.router','mainApp.contacts', 'mainApp.edit']);

angular.module('mainApp')
    .config(function ($urlRouterProvider, $stateProvider){

        $urlRouterProvider.otherwise('/');
        $stateProvider
            .state("home", {
                url: "/",
                templateUrl: "views/contact-list.html"
            }).state("edit", {
                url: "/edit",
                templateUrl: "views/edit-contact.html",
                params:{'_id':'','name':'','surname':'','tel':''}
            });
    });
})();
