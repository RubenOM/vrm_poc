'use strict';
(function () {
  angular.module('mainApp.edit', []);

  function editController($http,$stateParams, $state)
    {
      var vm = this;
      vm.add= true;
      vm.search = false;
      vm.pageData = {
            'shows': {
              'name':true,
              'nameSearch':false,
              'surname': true,
              'tel': true,
              'telSearch' : false
            },
            'contact':{
              'name':$stateParams.name,
              'surname':$stateParams.surname,
              'tel':$stateParams.tel
            }
          };

          vm.updateUser = function (event)
          {
            var payload = {
              '_id': $stateParams._id,
              'name': event.contact.name,
              'surname': event.contact.surname,
              'tel': event.contact.tel
            };
            $http.put('http://localhost:25000/contacts/'+$stateParams._id,payload).then(function (res)
            {
                 $state.go('home');
            });


        }
    };
    angular.module('mainApp.edit')
        .controller('editController', editController);
})();
