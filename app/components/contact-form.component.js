'use strict';
(function() {

  angular
    .module('mainApp')
    .component('contactForm', {
      templateUrl: 'components/contact-form.component.html',
      controller: contactForm,
      controllerAs: 'vm',
      bindings:{
        formname: '@',
        boton: '@',
        contact: '<',
        shows: '<',
        onSubmit: '&'
      }
    });

    function contactForm() {
      var vm = this;
      vm.$onChanges = function (changes) {
        if (changes.contact) {
          vm.contact = angular.copy(vm.contact);
        }

      };
      vm.submitForm = function () {
        vm.onSubmit({
          $event: {
            contact : vm.contact
          }
        });
      };
    }
})();
