'use strict';
(function () {
  angular.module('mainApp.contacts', []);

  function contactsController ($http,$state)
  {
      var vm = this
      vm.name = "Contactos Poc4Vermon";


      vm.$onInit = function() {
          vm.refreshForm();
      }

      vm.refreshForm = function(){
        vm.add= false;
        vm.search = false;
        vm.contacts = {};
        vm.pageData = {
              'shows': {
                'name':true,
                'nameSearch':false,
                'surname': true,
                'tel': true,
                'telSearch' : false
              }
        };

        $http.get('http://localhost:25000/contacts').then(function (res)
        {
            vm.contacts = res.data;
        });
      }

    vm.searchForm = function()
    {
      vm.add= false;
      vm.search = !vm.search;
      vm.pageData = {
            'shows': {
              'name':false,
              'nameSearch':true,
              'surname': false,
              'tel': false,
              'telSearch' : true
            }
          };
    }

    vm.addForm = function()
    {
      vm.add= !vm.add;
      vm.search = false;
      vm.pageData = {
            'shows': {
              'name':true,
              'nameSearch':false,
              'surname': true,
              'tel': true,
              'telSearch' : false
            }
          };
    }

    vm.addUser = function (event)
    {
      var payload = {
        'name': event.contact.name,
        'surname': event.contact.surname,
        'tel': event.contact.tel
      };

      $http.post('http://localhost:25000/contacts',payload).then(function (res)
      {
        alert('Contacto '+res.data.name+' creado correctamente');
         vm.contacts.push(res.data);
      });
  }

  vm.searchContact= function ($event) {
    var nombre = $event.contact.name === undefined ? '' :$event.contact.name;
    var tel = $event.contact.telSearch === undefined ? '' :$event.contact.telSearch;

    $http.get('http://localhost:25000/contacts?name_like='+nombre+'&tel_like='+tel).then(function (res)
    {
        vm.contacts = res.data;
    });
  }

  vm.delete = function(obj, index) {
    $http.delete('http://localhost:25000/contacts/'+obj._id).then(function (res)
    {
      vm.contacts.splice(index,1);
      alert('Se ha eliminado el contacto: '+obj.name);
    });
  }

  vm.edit = function(indice) {
    var contactEdit = {
      '_id':vm.contacts[indice]._id,
      'name':vm.contacts[indice].name,
      'surname':vm.contacts[indice].surname,
      'tel':vm.contacts[indice].tel
    };
    $state.go('edit',contactEdit);
  }

  };
    angular.module('mainApp.contacts')
        .controller('contactsController', contactsController);
})();
